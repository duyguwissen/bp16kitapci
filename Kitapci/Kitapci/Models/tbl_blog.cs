//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kitapci.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_blog
    {
        public tbl_blog()
        {
            this.tbl_blog_comment = new HashSet<tbl_blog_comment>();
        }
    
        public int ID { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Article { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Hit { get; set; }
    
        public virtual ICollection<tbl_blog_comment> tbl_blog_comment { get; set; }
    }
}
