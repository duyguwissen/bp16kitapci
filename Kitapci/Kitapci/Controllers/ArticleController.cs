﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 
using Kitapci.Models;
namespace Kitapci.Controllers
{
    public class ArticleController : Controller
    {
        //
        // GET: /Article/

        public ActionResult Index()
        {
            KitapciDBEntities aboutus = new KitapciDBEntities();
            var aboutusarticle = aboutus.tbl_article;
            tbl_article abtartcle = (from x in aboutusarticle where x.ID == 1 select x).FirstOrDefault(); ;
            ViewBag.title = abtartcle.Title;
            ViewBag.article = abtartcle.Article;

            return View();
        }
        public ActionResult Index(int page)
        {
            KitapciDBEntities privacy = new KitapciDBEntities();
            var privacyarticle = privacy.tbl_article;
            tbl_article termspolicy = (from x in privacyarticle where x.ID == 2 select x).FirstOrDefault();
            ViewBag.title = termspolicy.Title;
            ViewBag.article = termspolicy.Article;
            return View();
        }
        public ActionResult Index1(int page)
        {
            KitapciDBEntities terms = new KitapciDBEntities();
            var termsarticle = terms.tbl_article;
            tbl_article termofuse = (from x in termsarticle where x.ID == 3 select x).FirstOrDefault();
            ViewBag.title = termofuse.Title;
            ViewBag.article = termofuse.Article;
            return View();
        }
       
     

    }
}
