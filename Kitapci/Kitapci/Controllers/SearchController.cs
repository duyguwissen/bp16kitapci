﻿using Kitapci.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kitapci.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/

        public ActionResult Index(string q)
        {
            KitapciDBEntities db = new KitapciDBEntities();
            ViewBag.Aranan = q;

            //son cikan kitaplar
            IEnumerable<tbl_book> soncikanlist = (from s in db.tbl_book where s.Title.Contains(q) orderby s.PublishedDate descending select s).Take(100);
            int soncikansay= soncikanlist.Count();

            //bestseller kitaplar
            IEnumerable<tbl_book> bestsellerlist = (from s in db.tbl_book where s.Title.Contains(q) & s.RatingsCount>0 select s).Take(100);
            int bestsellersay = bestsellerlist.Count();

            //onerilen kitaplar
            IEnumerable<tbl_book> onerilenlist = (from s in db.tbl_book where s.Title.Contains(q) orderby s.AverageRating descending select s).Take(100);
            int onerilensay = onerilenlist.Count();

            ViewBag.Sayi = (soncikansay + bestsellersay + onerilensay);
            ViewBag.SonCikanlar = soncikanlist;
            ViewBag.Bestseller = bestsellerlist;
            ViewBag.Onerilenler = onerilenlist;

            ViewBag.SonCikanSay = soncikansay;
            ViewBag.BestSellerSay = bestsellersay;
            ViewBag.OnerilenSay = onerilensay;

            return View();
        }



    }
}
