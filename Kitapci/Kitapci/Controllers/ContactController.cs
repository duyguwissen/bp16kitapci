﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kitapci.Models;
namespace Kitapci.Controllers
{
    public class ContactController : Controller
    {

        
        //
        // GET: /Contact/

        public ActionResult Index()
        {
            KitapciDBEntities k =  new KitapciDBEntities();
            var ayar =( from a in k.tbl_settings select a).FirstOrDefault();  
            return View(ayar);
        }

    }
}
