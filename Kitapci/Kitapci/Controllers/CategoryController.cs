﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kitapci.Models;


namespace Kitapci.Controllers
{
    public class CategoryController : Controller
    {
        //
        // GET: /Category/

      

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CategoryDetail(string id)
        {
            ViewBag.CategoryName = id;
            KitapciDBEntities ctx = new KitapciDBEntities();
            IEnumerable<tbl_book> books = (from x in ctx.tbl_book_category_match where x.tbl_book_category.Category == id select x.tbl_book).Take(50);
            return View(books);
        }

    }
}
