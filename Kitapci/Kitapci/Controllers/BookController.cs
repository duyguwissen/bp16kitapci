﻿
using Kitapci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kitapci.Models;
namespace Kitapci.Controllers
{
    public class BookController : Controller
    {
        //https://developers.google.com/books/docs/v1/reference/volumes
        // GET: /Book/
        //https://bitbucket.org/
        //https://chrome.google.com/webstore/search/zenmate?hl=en-US

        KitapciDBEntities db = new KitapciDBEntities();

        public ActionResult Index()
        {
            return View();
        }



        public ActionResult BookDetail(string id)
        {
            string strID = Convert.ToString(id);
            tbl_book_author_match book = (from b in db.tbl_book_author_match where b.BookID == strID select b).SingleOrDefault();
            return View(book);
        }


    }
}
