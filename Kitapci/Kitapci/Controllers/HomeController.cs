﻿using Kitapci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 
namespace Kitapci.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {

            KitapciDBEntities db = new KitapciDBEntities();
            var last = db.tbl_book;
           
            IEnumerable<tbl_book> l = (from x in db.tbl_book orderby x.PublishedDate descending select x).Take(4);
            ViewBag.sonkitap = l;
            return View();
          
        }

    }
}
