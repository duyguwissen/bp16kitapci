﻿using Kitapci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kitapci.Controllers
{
    public class SharedController : Controller
    {
        //
        // GET: /Shared/

        KitapciDBEntities db = new KitapciDBEntities();

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult RelatedBook()
        { 
        
            var product = db.tbl_book;
            IEnumerable<tbl_book> relatedproduct = (from b in product
                                                    select b).OrderBy(c => Guid.NewGuid()).Take(8); 

            return View(relatedproduct);
        
        }

        
        public ActionResult BestSeller()
        {
            IEnumerable<tbl_book> sorgu = from b in db.tbl_book
                                          join s in db.tbl_book_bestseller
                                          on b.ID equals s.BookID
                                          select b;
            return View(sorgu);
 
        }

        public ActionResult Slider()
        {
            IEnumerable<tbl_slider> sorgu = from b in db.tbl_slider select b;
            return View(sorgu);

        }


        public ActionResult Comment()
        {
            var user = db.tbl_user;
            var comment = db.tbl_book_comment;
            IEnumerable<tbl_blog_comment> commnetofuser = from y in db.tbl_blog_comment
                                                         
                                                          select y;
            //  ViewBag.c = commnetofuser;
            return View(commnetofuser);
        }

        public PartialViewResult Menu()
        {
            var menuler = db.tbl_book_category;
            IEnumerable<tbl_book_category> m = (from x in menuler where x.IsMenu==true select x);
            return PartialView(m);
        }


        public ActionResult CategoryList()
        {
            IEnumerable<tbl_book_category> categorylist = from c in db.tbl_book_category
                                                          orderby c.Category
                                                          select c;
            return View(categorylist);
        }


        public ActionResult RandomBook()
        {
            //Random rnd = new Random();
            //int r = rnd.Next();
            IEnumerable<tbl_book_author_match> categorylist = (from b in db.tbl_book_author_match
                                                               select b).OrderBy(c => Guid.NewGuid()).Take(10);
            return View(categorylist);
        }




        public ActionResult PopularPosts()
        {

            IEnumerable<tbl_blog> blogpopular = (from b in db.tbl_blog
                                                               select b).Take(3);
            return View(blogpopular);
        }

        public ActionResult Blog()
        {

            IEnumerable<tbl_blog> blogpopular = (from b in db.tbl_blog orderby b.Hit descending
                                                 select b).Take(2);
            return View(blogpopular);
        }
    }
}
