﻿using Kitapci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kitapci.Controllers
{
    public class SideBarController : Controller
    {
        //
        // GET: /SİdeBar/
        KitapciDBEntities db = new KitapciDBEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Social()
        {
            KitapciDBEntities k = new KitapciDBEntities();
            var ayar = (from a in k.tbl_settings select a).FirstOrDefault();
            return View(ayar);
        
        }

        public ActionResult SidebarCategories()
        {
            KitapciDBEntities k = new KitapciDBEntities();
            IEnumerable < tbl_book_category > cats= from c in k.tbl_book_category select c;
            return View(cats);
        }


        public ActionResult PopularPost()
        {
            IEnumerable<tbl_blog> post = (from b in db.tbl_blog
                                          join s in db.tbl_blog_comment
                                          on b.ID equals s.BlogID
                                          select b).OrderByDescending(x => x.Hit).Take(3);
            
            return View(post);
        }

    }
}
