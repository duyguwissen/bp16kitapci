﻿using Kitapci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kitapci.Controllers
{
    public class AuthorController : Controller
    {
        //
        // GET: /Author/

        KitapciDBEntities db = new KitapciDBEntities();
        public ActionResult Index(string id)
        {
            string strID = Convert.ToString(id);
            ViewBag.AuthorName = strID;
            IEnumerable<tbl_book_author_match> book = (from b in db.tbl_book_author_match where b.tbl_author.Author == strID select b);
            return View(book);
        }

    }
}
