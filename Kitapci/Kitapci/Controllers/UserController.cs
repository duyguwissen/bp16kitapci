﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kitapci.Models;



namespace Kitapci.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/


        KitapciDBEntities kitapContext = new KitapciDBEntities();

        public ActionResult Index()
        {
     

            return View();
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(tbl_user k)
        {
            

            int sorgu = (from x in kitapContext.tbl_user where x.Email==k.Username & x.Password==k.Password select x).Count();;

            if (sorgu > 0)
            {
                var sessionTut = Session["User"];
                return Redirect("/Home/Index");
            }
           
            return View(sorgu); 
        }
    }
}
