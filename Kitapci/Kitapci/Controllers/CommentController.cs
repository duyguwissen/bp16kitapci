﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kitapci.Models;
namespace Kitapci.Controllers
{
    public class CommentController : Controller
    {
        //
        // GET: /Comment/

        KitapciDBEntities db = new KitapciDBEntities();

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult BookComment() {

            var bookComment = db.tbl_book_comment;
            IEnumerable<tbl_book_comment> comment = (from b in db.tbl_book_comment
                                                    select b).OrderByDescending(b=>b.Date).Take(3);
            return PartialView(comment);
        }
    }
}
