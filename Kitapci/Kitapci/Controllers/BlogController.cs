﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kitapci.Models;

namespace Kitapci.Controllers
{
    public class BlogController : Controller
    {
        //
        // GET: /Blog/

        public ActionResult Index()
        {
            KitapciDBEntities kitapContext = new KitapciDBEntities();
            IEnumerable<tbl_blog> sorgu = from x in kitapContext.tbl_blog select x;
          
            return View(sorgu);
        }

        public ActionResult BlogDetay(string id)
        {
            KitapciDBEntities kitapContext = new KitapciDBEntities();
            var blogdetay = kitapContext.tbl_blog;
            int intid = Convert.ToInt32(id);
            var blog_detayi = (from b in blogdetay where b.ID == intid select b).SingleOrDefault();
            return View(blog_detayi);
        }

    }
}
